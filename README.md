# AutoEdit #

AutoEdit is a WIP application that automatically edits together a video from given source files. Basically, this is a long term machine learning project of mine.


### Done ###

* Automatic cut detection for training data (Takes any video file and outputs cuts)

### To-Do ###
* Train model for specific style
* File analysis
* Editing environment (bulk of project)