from skimage.measure import structural_similarity as ssim
import cv2
import scipy
from scipy.optimize import minimize_scalar
import numpy as np
import math



vc = cv2.VideoCapture('got.mp4')
c=1

if vc.isOpened():
    rval , frame = vc.read()
else:
    rval = False

frames = []

SD = []
count = 0

while rval:
    rval, frame_1 = vc.read()
    rval, frame_2 = vc.read()

    frames.append(frame_1)
    frames.append(frame_2)

    if frame_2 is None:
        print("Second frame is null")

    hist_10 = cv2.calcHist([frame_1], [0], None, [256], [0, 256])
    hist_11 = cv2.calcHist([frame_1], [1], None, [256], [0, 256])
    hist_12 = cv2.calcHist([frame_1], [2], None, [256], [0, 256])

    hist_20 = cv2.calcHist([frame_2], [0], None, [256], [0, 256])
    hist_21 = cv2.calcHist([frame_2], [1], None, [256], [0, 256])
    hist_22 = cv2.calcHist([frame_2], [2], None, [256], [0, 256])

    hist_10 = cv2.normalize(hist_10).flatten()
    hist_11 = cv2.normalize(hist_11).flatten()
    hist_12 = cv2.normalize(hist_12).flatten()
    hist_20 = cv2.normalize(hist_20).flatten()
    hist_21 = cv2.normalize(hist_21).flatten()
    hist_22 = cv2.normalize(hist_22).flatten()

    diff_0 = abs(hist_10 - hist_20).sum(axis=0)
    diff_1 = abs(hist_11 - hist_21).sum(axis=0)
    diff_2 = abs(hist_12 - hist_22).sum(axis=0)

    diff = diff_0 + diff_1 + diff_2

    SD.append(diff)
    count += 1
    print("reading...", count)
    cv2.waitKey(2)
vc.release()
print("done reading")

W = int(max(SD))
print(W)
f = [0] * W

for i in range(0, W):
    for j in range(0, len(SD)):
        if int(SD[j]) == i:
            f[i] += 1
print f

def accumulate(input_list, start_index, end_index):
    total = 0
    input_list = list(input_list[slice(start_index, end_index)])
    for item in input_list:
        total += item
    return total

def p_Zero(T):
    return accumulate(f, 0, int(T))

def p_One(T):
    return accumulate(f, int(T), W)

def h_NS(T_plus_one):
    T = int(T_plus_one) - 1
    if p_Zero(T+1) == 0:
        return 0
    elif p_One(T+1) == 0:
        return 0
    elif p_Zero(T) == 0:
        return 0
    elif p_Zero(T) == 0:
        return 0
    else:
        return ((p_Zero(T)/p_Zero(T+1)) * h_NS(T)) - ((f[T+1] / p_Zero(T+1)) * math.log((f[T+1] / p_Zero(T+1)))) - ((p_Zero(T)/p_Zero(T+1)) * math.log((p_Zero(T)/p_Zero(T+1))))

def h_S(T_plus_one):
    T = int(T_plus_one) - 1
    if p_Zero(T+1) == 0:
        return 0
    elif p_One(T+1) == 0:
        return 0
    elif p_Zero(T) == 0:
        return 0
    elif p_Zero(T) == 0:
        return 0
    else:
        return ((p_One(T)/p_One(T+1)) * h_NS(T)) - ((f[T+1] / p_One(T+1)) * math.log((f[T+1] / p_One(T+1)))) - ((p_One(T)/p_One(T+1)) * math.log((p_One(T)/p_One(T+1))))

def total_H(T):
    return -1 * (h_NS(T) + h_S(T))

def find_T():
    return minimize_scalar(total_H, bounds=(0, W), method='bounded')

T = find_T().x
print(T)


for index, frame in enumerate(SD):
    print(index + " for " + frame)
    if frame > T:
        cut = frames[index]
        print("Outputing")
        cv2.imwrite(str(c) + '.jpg', cut)



    # if cut:
    #
    #cv2.imwrite(str(c) + '.jpg',frame_1)
    # c = c + 2
    # cv2.waitKey(2)
